
"""
Train script of the battlefield game
"""

import argparse
import time
import torch
import logging as log
import math
import random

import numpy as np

from pettingzoo.magent import battlefield_v4
from MAA2C import MAA2C


if __name__ == "__main__":


    parser = argparse.ArgumentParser()

    parser.add_argument('--seed', type=int, default=4, metavar='N',
                        help='random seed (default: 4)')
    parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                        help='batch size (default: 128)')
    parser.add_argument('--num_steps', type=int, default=500, metavar='N',
                        help='max episode length (default: 1000)')
    parser.add_argument('--num_episodes', type=int, default=1000, metavar='N',
                        help='number of episodes (default: 1000)')
    parser.add_argument('--hidden_size', type=int, default=128, metavar='N',
                        help='number of episodes (default: 128)')
    parser.add_argument('--updates_per_step', type=int, default=5, metavar='N',
                        help='model updates per simulator step (default: 5)')
    parser.add_argument('--replay_size', type=int, default=1000000, metavar='N',
                        help='size of replay buffer (default: 1000000)')
    parser.add_argument('--render', type=bool, default=False, metavar='R',
                        help='whether or not to render env (default: False)')
    parser.add_argument('--model_path', default="",
                        help="relative path of the saved model")

    args = parser.parse_args()

    # init the game
    env = battlefield_v4.env()
    env.seed(args.seed)
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)
    env.reset()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    observation_space_shape = env.observation_space.shape

    rewards = []
    total_numsteps = 0
    updates = 0

    agent = MAA2C(env=env, memory_capacity=args.replay_size,
              state_dim=env.state.shape[0], action_dim=env.action_space('blue_0'),
              batch_size=args.batch_size, entropy_reg=0,
              done_penalty=0, roll_out_n_steps=50,
              reward_gamma=0.99,
              epsilon_start=0.99, epsilon_end=0.05,
              epsilon_decay=600, max_grad_norm=None,
              episodes_before_train=100,
              critic_loss="mse")

    for i_episode in range(args.num_episodes):
        state = torch.Tensor([env.reset()])
        episode_reward = 0
        while True:
            for agent in env.agent_iter():
                total_numsteps += 1
                episode_reward += reward

                observation, reward, done, info = env.last()
                if 'red' in agent:
                    num = len(observation[0])
                    action = np.random.randint(0, 20, dtype=np.int32)
                elif 'blue' in agent:
                    action = agent.exploration_action(observation)
                    action = action.numpy()[0]
                else:
                    AssertionError

                action_t = torch.Tensor(action)
                mask = torch.Tensor([not done])
                observation = torch.Tensor([observation])
                reward = torch.Tensor([reward])


                env.step(action)

                agent.memory.push(state, action_t, mask, observation, reward)
                state = observation

                total_numsteps += 1
                episode_reward += reward

                if done:
                    print("episode ", i_episode, info["completed"], info["crashed"], info["max_steps"],
                          info["position"], episode_reward)
                    agent.n_episodes += 1
                    agent.train()
                    break



    env.close()